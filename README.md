# VIVA #

  **V**MD **I**sosurface **V**ideo **A**utomaton      
 
  An interactive shell script for producing 
  movies of isosurfaces from VMD trajecories. 

  VIVA started out as a hacked-together script to automate the process of making
  movies using the freely available VMD package (http://www.ks.uiuc.edu/Research/vmd/).
  While VMD already has some plugins for making movies, there was no functionality to
  make movies from multiple sets of gridded data (in my case density cubes). 
  Additionally, I wanted to use my own settings for rendering the still frames, and 
  for stitching them together into a movie. 
 
  This hacked together helper script grew in to a more fully featured interactive 
  shell script when I began sharing it with others in my group and collaborators. 
  At this point there's many features I still want to add, and I also want to introduce 
  some sort of input script, for those who don't want to deal with interactive prompts.
  There is a branch in this repository where I've started porting the code over to Ruby.
  This should allow me to better progress on this feature wish-list.
  

  USAGE: sh viva.sh
  and follow the interactive prompts!
 
  TO DO: lots more functionality - probably
  will need a rewrite in Ruby. 
 
  Send comments, questions praise and abuse 
  to conrad.s.johnston [at] googlemail.com
 
 ---------------------------------------------------------------------------

  VIVA: An interactive shell script for producing VMD movies
  Copyright (C) 2017 Conrad Johnston

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ---------------------------------------------------------------------------
