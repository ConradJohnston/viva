#!/bin/bash
# ---------------------------------------------------------------------------
#
#  VIVA: An interactive shell script for producing VMD movies
#  Copyright (C) 2017 Conrad Johnston
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program in the file, "LICENSE.txt".
#  If not, see <http://www.gnu.org/licenses/>.
#
# ---------------------------------------------------------------------------
#
#   ******************************************
#    ____    ____  __  ____    ____  ___      
#    \   \  /   / |  | \   \  /   / /   \     
#     \   \/   /  |  |  \   \/   / /  ^  \    
#      \      /   |  |   \      / /  /_\  \   
#       \    /    |  |    \    / /  _____  \  
#        \__/     |__|     \__/ /__/     \__\ 
#                                             
#   ******************************************
#         VMD Isosurface Video Automaton      
# 
#  An interactive shell script for producing 
#  movies of isosurfaces from VMD.
# 
#  USAGE: sh viva.sh
#  and follow the interactive prompts!
# 
#  TO DO: lots more functionality - probably
#  will need a rewrite in Ruby. 
# 
#  Send comments, question praise and abuse 
#  to conrad.s.johnston@googlemail.com
# 

isosurface() {
   
    checkdependancies 

    cubesnotok=true

    while $cubesnotok;  
    do
        clear; printmainbanner
        printstepbanner "Finding Cube Files"
        
        # Prompt for input cube files
        printf " Enter a string to search for, common to all desired cube files:\n "
        # Loop until a valid search string is given
        while read cubesearchstring; do
            # Test if the search returns an empty string
            if [ "" = "$(find . -maxdepth 1 -name "*$cubesearchstring*")" ] 
            then
                printf "\n ERROR: No cube files found!\n"
                printf " Enter a string to search for, common to all desired cube files:\n "
                continue 
            else
                break
            fi
        done
        
        # Find matching cube files and print them
        printf "\n Found these files:\n"
        for ff in $(find . -maxdepth 1 -name "*$cubesearchstring*" | sort -V ); do
            echo "    " $(basename ${ff})
        done
        
        #Count the number of cubefiles for later
        toti=$(find . -maxdepth 1 -name "*$cubesearchstring*" | wc -l )
        
        # Confirm and continue
        printf "\n Are these cube files correct? [y/n/quit]\n "
        while read options; do
              case "$options" in
                  y) cubesnotok=false ; break ;;
                  n) break ;;
               quit) exit ;;
                  *) printf " Invalid option. Are these cube files correct? [y/n/quit]\n " ;;
              esac
        done
    done
    
    memnotok=true
    while $memnotok;  
    do
        # Check the memory required (units of MB) 
        reqmem=$(du -c --max-depth=1 *$cubesearchstring* | tail -1 | awk '{printf "%.2f", $1/1024}')  
        freemem=$(free | awk '/^\-/{printf "%.2f", $NF/1024}')
        # Print the memory check results
        printf "\n The memory required by VMD will be $reqmem MB \n" 
        printf " The free system memory is $freemem MB \n"
        
        # Give the user some direct feedback on the consequences of the memory situation
        if (( $(echo "$reqmem < $freemem" |bc -l) )) 
        then  
            # There is enough free, but how much wiggle room?
            if (( $(echo "$reqmem > $freemem*0.9" |bc -l) )) 
            then
                # Less than 10% wiggle
                printf "\n \033[1;33m\033[107mWARNING: This probably be okay, but will use more than 90%% of the available memory\033[0m\n\n"
            else
                # Plenty
                printf "\n \033[0;34m\033[107mThis looks okay\033[0m\n"
             fi
        else 
            # Not enough memory - fatal error?
            printf "\n \033[0;31m\033[107mWARNING: I've got a bad feeling about this...\033[0m\n"
        fi
        
        # Ask for permission
        printf "\n Do you want to continue loading the cube files? [y/n/recheck] \n "
        while read options; do
              case "$options" in
                  y) memnotok=false ; break ;;
                  n) printf "\n Please try a smaller number of cube files or the \`--lowmem\` option.\n"
                     printf "\n EXITING...\n" ; exit 2 ;;
           recheck) break ;;
                  *) printf "\n Invalid option. Is this memory use okay? [y/n]" ;;
              esac
        done
    done  

    # Setting up VMD
    clear; printmainbanner 
    printstepbanner "Setting up VMD"
    
    if TCLCubeTrajGen $cubesearchstring ;
    then
        printf "\n VMD input file prepared sucessfully.\n"
    else
        printf "\n FATAL ERROR: Failed to prepare the VMD input file.\n"
        exit 1
    fi


    vmdtempoutfile=$(mktemp)
#    vmdtempoutfile=testvmdout.out
#    script --return -c "vmd -e $TCLFILE &>vmdtempfile" /dev/null &>/dev/null
   
    printf "\n Once VMD has started, set up your scene in VMD as you would" 
    printf " like it to appear in your movie.\n"
    printf " When you are happy and ready to make your movie, return to"
    printf " this terminal.\n"

    usenotready=true
    while $usernotready; do
        printf " Start VMD and load cube files? [y/n/quit]\n " 
        while read options; do
           case "$options" in
               y) usernotready=false ; break ;;
               n) break ;;
            quit) exit ;;
               *) printf " Invalid option. Are you ready to start VMD? [y/n/quit]\n " ;;
           esac
        done
    done

    printf "\n Starting VMD... "
    screen -d -m -S vmdscreen sh -c "vmd -e $TCLFILE &> $vmdtempoutfile"
    printf "\r Starting VMD... DONE\n"
    
   
   # PARK THIS FOR LATER - HAVE TO THINK ABOUT HOW TO GET ERROR CODES FROM SCREEN
   # if screen -d -m -S vmdscreen sh -c "vmd -e $TCLFILE &> vmdtempoutfile" ; then
   #     printf "\r Starting VMD... DONE\n"
   # else
   #     printf "\n FATAL ERROR : Couldn't start VMD.\n"
   #     exit 1
   # fi

    printf "\n Loading cube files... \n"
 
    preparebar $(($(tput cols)-4)) "="
    ii=0
    starttime=$(gettime)
    while [ $ii -lt $toti ] ; do
        timeremaining=$(echo $(timeestimate $starttime $ii $toti))
        progressbar $ii $toti $timeremaining
        ii=$(grep $vmdtempoutfile -e "Info) Finished with coordinate file" | wc -l)
    done
    progressbar $toti $toti "0"

    printf "\n\n\n Cube files loaded.\n"

    printf "\n Set up your scene in VMD as you would" 
    printf " like it to appear in your movie.\n"
    
    scenenotready=true
    while $scenenotready; do
        printf " Are you ready to commit to this scene. [y/n/quit]\n " 
        while read options; do
           case "$options" in
               y) scenenotready=false ; break ;;
               n) break ;;  
            quit) exit ;;
               *) printf " Invalid option. Are you ready to continue? [y/n/quit]\n " ;;
           esac
        done
    done

    clear; printmainbanner 
    printstepbanner "Capturing movie frames"

#   echo "Ready to continue!"

    screen -S vmdscreen -X stuff "^M"

    GETFRAMESFILE=$(mktemp)

    cat > $GETFRAMESFILE <<-EOF
    proc getframes {{step 1} {firstframe 0} {lastframe {molinfo top get numframes}}} {
        # The numbers next to arguments specifies their default values
        # To stop at frame n, n+1 must be passed as the loop runs up to the index given
        
        set tempout [open $vmdtempoutfile a] 

        animate goto start
        
        if {\$lastframe == 0} {
            return -code error "No frames loaded"
        } else {
            
            if {\$lastframe eq "molinfo top get numframes"} {
                set lastframenumeric [eval \$lastframe]
            } else {
                set lastframenumeric \$lastframe
            }

           for {set ii \$firstframe} {\$ii < \$lastframenumeric} {incr ii \$step} {      
                animate goto \$ii
                display update
                #The strange "" in the command below is passed to Tachyon. Tachyon doesn't know
                #what to do with do so exits. This way we can generate only dat files with no
                #rendering.
                render Tachyon frame.[format "%07d" [expr \$ii]].dat ""
                puts \$tempout "Tachyon dat prepared"
                flush \$tempout 
           }
           puts \$tempout "###All Tachyon dats prepared###"
           flush \$tempout 
        }
    }
EOF
    
    screen -S vmdscreen -X stuff "source $GETFRAMESFILE^M"
    screen -S vmdscreen -X stuff "getframes^M"

    printf "\n Creating Tachyon frames... \n"
    preparebar $(($(tput cols)-4)) "="
    ii=0
    starttime=$(gettime)
    while [ $ii -lt $toti ] ; do
        timeremaining=$(echo $(timeestimate $starttime $ii $toti))
        progressbar $ii $toti $timeremaining
        ii=$(grep $vmdtempoutfile -e "Info) Tachyon file generation finished" | wc -l)
    done
    progressbar $toti $toti "0"

    printf "\n\n\n Tachyon frames ready.\n"
    
    clear; printmainbanner 
    printstepbanner "Capturing movie frames"

    # Until ensures that this isn't skipped due to tmp's slow I/O
    until $(grep -q $vmdtempoutfile -e "###All Tachyon dats prepared###") 
    do
        printf "\r Killing VMD..."
    done

   #debughold

    screen -S vmdscreen -X stuff "exit^M"
    printf "\r Killing VMD...DONE\n"

    renderframes y y
    
   #debughold

    # Defualt video settings
    filename="video.mp4"
    duration=15
    height=1024
    width=1024
    keepjpg="y"
    
    videosettingsnotok=true   

    while $videosettingsnotok; do 
        clear; printmainbanner
        printstepbanner "Making the movie"
        printf "\n" 
        printf "\n The current video settings are as follows: \n"
        printf "\n           Filename : $filename" 
        printf "\n    Duration (secs) : $duration" 
        printf "\n    Height (pixels) : $height" 
        printf "\n     Width (pixels) : $width" 
        if [ "$keepjpg" == "n" ] ; then 
            printf "\n    Keep JPG stills : No"
        else
            printf "\n    Keep JPG stills : Yes"
        fi
        printf "\n" 

        # Confirm the  settings
        printf "\n Are these settings okay? [y/n]\n "
        while read options; do
              case "$options" in 
                  y) videosettingsnotok=false; notready=false ; break 2 ;;
                  n) break ;;
                  *) printf " Invalid option. Are these settings okay? [y/n]\n " ;;
              esac
        done
        
        notready=true
        while $notready; do 
            # Confirm the  settings
            printf "\n Filename: $filename"
            printf "\n Okay? [y/n]\n "
            while read options; do
                  case "$options" in
                      y) 
                        notready=false ; break ;;
                      n) 
                        printf "\n Type new value:\n "
                        read filename 
                        break;;
                      *) 
                        printf " Invalid option. Is this setting okay? [y/n]\n " ;;
                  esac
            done
        done
       
        notready=true
        while $notready; do 
            # Confirm the  settings
            printf "\n Duration: $duration"
            printf "\n Okay? [y/n]\n "
            while read options; do
                  case "$options" in
                      y) 
                        notready=false ; break ;;
                      n) 
                        printf "\n Type new value:\n "
                        read duration 
                        break;;
                      *) 
                        printf " Invalid option. Is this setting okay? [y/n]\n " ;;
                  esac
            done
        done
        
        notready=true
        while $notready; do 
            # Confirm the  settings
            printf "\n Height: $height"
            printf "\n Okay? [y/n]\n "
            while read options; do
                  case "$options" in
                      y) 
                        notready=false ; break ;;
                      n) 
                        printf "\n Type new value:\n "
                        read height 
                        break;;
                      *) 
                        printf " Invalid option. Is this setting okay? [y/n]\n " ;;
                  esac
            done
        done
        
        notready=true
        while $notready; do 
            # Confirm the  settings
            printf "\n Width: $width"
            printf "\n Okay? [y/n]\n "
            while read options; do
                  case "$options" in
                      y) 
                        notready=false ; break ;;
                      n) 
                        printf "\n Type new value:\n "
                        read width 
                        break;;
                      *) 
                        printf " Invalid option. Is this setting okay? [y/n]\n " ;;
                  esac
            done
        done
        
        notready=true
        while $notready; do 
            # Confirm the  settings
            printf "\n Keep JPGs: $keepjpg"
            printf "\n Okay? [y/n]\n "
            while read options; do
                  case "$options" in
                      y) 
                        notready=false ; break ;;
                      n) 
                        printf "\n Type new value:\n "
                            while read value; do
                                case "$value" in
                                    y) keepjpg="y" ; break ;;
                                    n) keepjpg="n" ; break ;;
                                    *) printf " Invalid option. Enter value [y/n]\n " ;;
                                esac
                            done
                            break;;
                      *) 
                        printf " Invalid option. Is this setting okay? [y/n]\n " ;;
                  esac
            done
        done
    done

    # Catch filename if already exists
    if [ -f "$filename" ] 
    then
        until [ ! -f "$filename" ] ; do
            notready=true
            while $notready; do
                printf "\n WARNING: Filename, \"$filename\", already exists!\n"
                printf " Choose a new filename:\n "
                read filename 

                # Confirm the  settings
                printf "\n New filename: $filename"
                printf "\n Okay? [y/n]\n "
                while read options; do
                      case "$options" in
                          y) 
                            notready=false ; break ;;
                          n) 
                            printf "\n Type new value:\n "
                            read filename 
                            break;;
                          *) 
                            printf " Invalid option. Is this filename okay? [y/n]\n " ;;
                      esac
                done
            done
        done
    fi

    framerate=$(($toti/$duration))

    if rendermovie $filename $width $height $framerate $keepjpg
    then
        printf "\n Movie rendered successfully! \n\n"
        exit 0
    else
        printf "\n\n FATAL: Couldn't render movie! \n\n"
        exit 1
    fi



}

TCLCubeTrajGen() {
    # ============================================================================
    # TCLCubeTrajGen [BASE FILE NAME]
    # 
    # Function to generate a TCL input for VMD to create trajectories of cube files.
    # Finds all cube files in the current directory partially 
    # matching"BASE FILE NAME" and generates a TCL file accordingly.
    #
    # Arguments > BASE FILE NAME - The root filename of the cube files to be 
    #                              included.
    # ============================================================================
    CWD=$PWD                    
    cubename=$1                 
    
    # Make temp files
#    TFILE=$(mktemp)
    TCLFILE=./tclcubefile.dat
    
    printf " Generating VMD script..."
    printf "\r Generating VMD script... DONE\n"
    
    # Create the TCL script by piecing together the component parts:
    cat > $TCLFILE <<-'EOF' 
        # Display settings
        display projection   Orthographic
        display nearclip set 0.000000
        display farclip  set 10.000000
        display depthcue   off
        
        # store the molecule id for later use
EOF
   
    # Generate the first item in the TCL list of cube files
    firstcube=$(basename $(find . -maxdepth 1 -name "*$cubename*" | sort -V | head -1))
    echo "        set updmol [mol new {$firstcube} type cube waitfor all]" >> $TCLFILE 
    
    # Generate the rest
    for ff in $(find . -maxdepth 1 -name "*$cubename*" | sort -V | tail -n +2); do
        ncube=$(basename ${ff})
        echo "        mol addfile {$ncube} type cube waitfor all" >> $TCLFILE
    done

    cat >> $TCLFILE <<-'EOF2'
        mol delrep 0 top
        mol representation CPK 1.000000 0.300000 40.000000 40.000000
        mol color Name
        mol material Glossy
        mol addrep top
        mol color ColorID 0
        mol material Transparent
        mol representation Isosurface 0.0005 0.0 0.0 0.0
        mol addrep top
        mol selection {all}
        mol color ColorID 7
        mol material Transparent
        mol representation Isosurface -0.0005 0.0 0.0 0.0
        #mol selection {all}
        mol addrep top
        # store name of the isosurface representation (id=3) for later use
        # updrep1 is the positive surface, and updrep2 is the negative
        set updrep1 [mol repname top 1]
        set updrep2 [mol repname top 2]
        mol rename top {System}
        
        rotate x by -90
        rotate y by 180
        scale by 1.8
        
        # use the volumetric data set for the isosurface corresponding to the frame.
        # $updmol contains the id of the molecule and $updrep the (unique) name of 
        # the isosurface representation
        #
        proc update_iso {args} { 
            global updmol
            global updrep1
            global updrep2
        
            # get representation's id from name and return if invalid
            set repid1 [mol repindex $updmol $updrep1]
            set repid2 [mol repindex $updmol $updrep2]
            if {$repid1 < 0} { return }
            if {$repid2 < 0} { return }
        
            # update representation but replace the data set 
            # id with the current frame number.
            set frame [molinfo $updmol get frame]
            #assign the isosurface config to rep
            lassign [molinfo $updmol get "{rep $repid1}"] rep
            mol representation [lreplace $rep 2 2 $frame]
            mol color ColorID 0
            mol modrep $repid1 $updmol
            lassign [molinfo $updmol get "{rep $repid2}"] rep
            mol representation [lreplace $rep 2 2 $frame]
            mol color ColorID 7
            mol modrep $repid2 $updmol
        }
        
        trace variable vmd_frame($updmol) w update_iso
        
        # Loop over frames and wrap the cube file
        set nf [molinfo top get numframes] 
        for {set i 0} {$i < $nf} {incr i} { 
            #Rewrap cube file and recalculate bonds
            animate goto $i
        #    pbc wrap -compound res -all 
         #   mol bondsrecalc $updmol
        }
        ## Alternative routine to update bonds (http://guido.vonrudorff.de/vmd-rcalculate-bonds-during-animation/)
        proc adj_bonds {args} { 
            mol bondsrecalc [lindex $args 1]
        }
        trace add variable ::vmd_frame write adj_bonds
        
        animate goto 0 
EOF2
    
    return 0
}

checkdependancies() {
   # need to check for:

   #vmd 
   if ! which vmd ; then
       printf "\n FATAL ERROR : Application \"vmd\" not found.\n"
       exit 1
   fi

   #Tchyon for rendering stills
   if ! which /opt/vmd/tachyon_LINUXAMD64 ; then
       printf "\n FATAL ERROR : Application \"tachyon\" not found.\n"
       exit 1
   fi
  
   # screen - screen seems to return the error code 1 for version (!?) so 
   # logic is reversed
   if ! which screen ; then
       printf "\n FATAL ERROR : Application \"screen\" not found.\n"
       exit 1
   fi
   
   # binary calculator for some binary maths
   if ! which bc ; then
       printf "\n FATAL ERROR : Application \"bc\" not found.\n"
       exit 1
   fi
   
   # awk - used for all sorts
   if ! which awk ; then
       printf "\n FATAL ERROR : Application \"awk\" not found.\n"
       exit 1
   fi
}



printmainbanner() {
    printf " ******************************************\n"
    printf "  ____    ____  __  ____    ____  ___      \n"
    printf "  \   \  /   / |  | \   \  /   / /   \     \n"
    printf "   \   \/   /  |  |  \   \/   / /  ^  \    \n"
    printf "    \      /   |  |   \      / /  /_\  \   \n"
    printf "     \    /    |  |    \    / /  _____  \  \n"
    printf "      \__/     |__|     \__/ /__/     \__\ \n"
    printf "                                           \n"
    printf " ******************************************\n"
    printf "       VMD Isosurface Video Automaton      \n"
    printf "\n"
}

printlicensebanner(){

   printf " VIVA Copyright (C) 2016 Conrad Johnston\n"
   printf " This program comes with ABSOLUTELY NO WARRANTY.\n"
   printf " This is free software, and you are welcome to\n"
   printf " redistribute it under certain conditions. \n"
   printf " Run 'sh viva.sh --license' for details.\n"
   printf "\n"
}   

printstepbanner() {
    
    printf "%*s" "$(tput cols)" | tr ' ' "=" 
    printf "\n"
    printf " $1 \n"
    printf "%*s" "$(tput cols)" | tr ' ' "="
    printf "\n"
}
    
notify() {
    if $HASnotifysend ; then
        notify-send "$1"
    fi
}

# These two functions are from a StackOverflow user, "Zarko Zivanov". 
preparebar() {
    # $1 - bar length
    # $2 - bar char
    # Conrad : I've taken away some spaces to allow for the numerical 
    # percentage that will update next to the bar
    barlen=$(($1-12)) 
    barspaces=$(printf "%*s" "$1")
    barchars=$(printf "%*s" "$1" | tr ' ' "$2")
}

progressbar() {
    # $1 - number (-1 for clearing the bar)
    # $2 - max number
    # $3 - estimated time remaining (milliseconds)
    if [ $1 -eq -1 ]; then
        printf "\r  $barspaces\r"
    else
        barch=$(($1*barlen/$2))
        barsp=$((barlen-barch))
        numericalrep=$(echo $1 $2 | awk '{ printf "%2.1f", ($1/$2)*100.0}')
        # Conrad - Added a time estimate
        
        #Get the estimate 
        totsecs=$3
        if [ "$totsecs" == "nil" ] 
        then 
            timeremaining="unknown"
        else
            ((hrs=($totsecs/1000)/3600, mins=($totsecs/1000)/3600%60, secs=($totsecs/1000)%60))
            if [ "$hrs" -gt "99" ] ; then
                hrs=99
            fi
            timeremaining=$(printf "%02d:%02d:%02d" $hrs $mins $secs)
        fi
        printf "\r [%.${barch}s%.${barsp}s] %s %%\r" "$barchars" "$barspaces" "$numericalrep"
        printf "\n Time remaining: %s\r\033M" "$timeremaining"
    fi
}

timeestimate() {
# $1 - starttime in milliseconds since Unix epoch
# $2 - the number of comleted iterations/the current iteration
# $3 - the total number of iterations needed.

     # Get the current time:
     currenttime=$(gettime)
     # Get the start time, current iteration, and required iterations
     starttime=$1
     ii=$2
     toti=$3
     
     if [ "$ii" -eq "0" ]
     then
         remainingtime="nil"
     else
         # Calc average time taken per iteration
         avgtime=$(echo | awk "{ print ($currenttime-$starttime)/$ii}")
         # Calc number of iterations left
         ((itersleft=$toti-$ii))
         # Calc an estimate of the remaining time
         remainingtime=$(echo $avgtime $itersleft | awk '{ printf "%.0f", $1*$2}') 
     fi

     echo $remainingtime
}

renderframes() {
    # Renders all frames
    # $1 - "y" to preserve the .dat, or else "n"
    # $2 - "y" to preserve the .tga, or else "n"
    # Render many frames with Tachyon in a loop
    
    printf "\n Rendering movie frames... \n"
    # Set up the timers and progress bars
    preparebar $(($(tput cols)-4)) "="
    counter=0
    starttime=$(gettime)
    totf=$(find . -type f -wholename "./frame.*.dat" | wc -l)
    
    #Loop over the frames to be rendered and update the progress bars.
    for framename in $(find . -type f -wholename "./frame.*.dat" -printf "%f\n" | sort -V); do
        rendersingleframe $framename 2048 2048 $1 $2  
        timeremaining=$(echo $(timeestimate $starttime $counter $totf))
        progressbar $counter $totf $timeremaining
        ((counter++))
    done
    progressbar $totf $totf "0"

    printf "\n\n\n Movie frames have been rendered.\n"
    
}

rendersingleframe() {
    # Renders a single frame
    # $1 - name of the dat frame to render
    # $2 - output X dimension in pixels
    # $3 - output Y dimension in pixels
    # $4 - "y" to preserve the .dat, or else "n"
    # $5 - "y" to preserve the .tga, or else "n"
     
    framename=$1
    xdim=$2
    ydim=$3
    keepdat=$4
    keeptga=$5
    
    # Render the frame 
    if ! /opt/vmd/tachyon_LINUXAMD64 -aasamples 12 $framename -format TARGA -res $xdim $ydim -o $framename.tga > /dev/null 
    then
        echo "FATAL ERROR: Could not render the Tachyon frame"
        exit 1
    fi

    # Convert the frame to JPG for ffmpeg later
    if ! mogrify -format jpg $framename.tga 
    then 
        echo "FATAL ERROR: Could not convert the TGA to JPG"
        exit 1
    fi

    # Remove the .dat files if requested
    if [ "$keepdat" == "n" ] 
    then 
        rm -f $framename
    fi
    
    # Remove the .tga files if requested
    if [ "$keeptga" == "n" ] 
    then 
        rm -f $framename.tga
    fi

}

rendermovie() {
    # Renders the movie
    # $1 - output movie name
    # $2 - output X dimension in pixels
    # $3 - output Y dimension in pixels
    # $4 - movie input framerate
    # $5 - "y" to preserve the .jpg, or else "n"
     
    moviename=$1
    xdim=$2
    ydim=$3
    framerate=$4
    keepjpg=$5

    #Get the first frame filename's number:
    firstframe=$(find . -name "frame*.dat.jpg" -printf '%f\n' | sort | head -1 | tr -d [:alpha:].)
    
    # Now stitch the frames together into a video
    if ! ffmpeg -loglevel panic -framerate $framerate -start_number $firstframe -i frame.%07d.dat.jpg -c:v libx264 -s:v $xdim:$ydim -crf 18 -preset veryslow -r 30 $moviename
    then
        echo "FATAL ERROR: ffmpeg failed to make the movie"
        exit 1
    fi

    # Remove the .jpg files if requested
    if [ "$keepjpg" == "n" ] 
    then 
        rm -f frame.*.dat.jpg
    fi
}    

gettime() {
    # Return the time in milliseconds. On BSD-like systems 
    # the trailing three figures are made zeros if the 
    # GNUdate app is not installed.
    echo $(($(date +'%s * 1000 + %-N / 1000000')))
}

printlicense(){

 echo "
 VIVA: An interactive shell script for producing VMD movies
 Copyright (C) 2016 Conrad Johnston
 Email: conrad.s.johnston@googlemail.com
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program in the file, "LICENSE.txt".
 If not, see <http://www.gnu.org/licenses/>.
 "
}

debughold(){

    notready=true
    while $notready; do
        printf " DEBUG: Continue? [y/n/quit]\n " 
        while read options; do
           case "$options" in
               y) scenenotready=false ; break ;;
               n) break ;;  
            quit) exit ;;
               *) printf " Invalid option. Are you ready to continue? [y/n/quit]\n " ;;
           esac
        done
    done
}

main(){
     
     clear ; printmainbanner ; printlicensebanner
     printstepbanner "Select Mode"
     printf "\n "
     printf "     1) Make isosurface movie\n "
     printf "     2) Make standard trajectory movie (Not yet implemented)\n "
     # Confirm and continue
     printf "\n Choose a mode: [1/2/quit]\n "
     while read options; do
           case "$options" in
               1) isosurface ; break ;;
               2) printf "\n ERROR: Mode not yet available!\n" ; exit 1 ;;
            quit) exit 0 ;;
               *) printf " Invalid option. Choose a mode [1/2/quit]\n " ;;
           esac
     done
}

## Check for command line flags, else run the main code

if [ "$1" == "--license" ]
then
    printmainbanner
    printlicense
else 
    main
fi

